import os
import json
import datetime
import matplotlib.pyplot as plt
import numpy as np
import math

# grafici
plt.rcParams['figure.figsize'] = [12, 2]
plt.rcParams['figure.dpi'] = 200  # 200 e.g. is really fine, but slower
plt.rcParams['xtick.labelsize'] = 3


def getDataFromFile(path):
    fromFile = []
    x = {}
    date = ""
    intDate = 0
    for items in os.listdir(path):
        if items.endswith('.json'):
            date = os.path.splitext(items)
            with open(path + "/" + items, 'r') as fi:
                x = json.load(fi)
                x.update({"date": str(datetime.datetime.fromtimestamp(int(date[0].split("-")[2]) // 1000))})
                fromFile.append(x)
            #                fromFile.append(json.load(fi))
    return fromFile


def getDataFromFile(path, fromTime="0", toTime="0", valid="both", profilesToCollect=0):
    fromFile = []
    x = {}
    date = ""
    intDate = 0
    profilesCounter = 0

    if toTime == "0":
        now = datetime.datetime.now()
        toTime = now.strftime("%Y-%m-%d %H:%M:%S")
    if fromTime == "0":
        fromTime = "1970-1-1 00:00:00"

    for items in os.listdir(path):
        if items.endswith('.json'):
            date = os.path.splitext(items)
            if len(date[0].split("-")) > 1:
                if datetime.datetime.fromtimestamp(int(date[0].split("-")[2]) / 1e3) > datetime.datetime.strptime(
                        fromTime, "%Y-%m-%d %H:%M:%S") and datetime.datetime.fromtimestamp(
                    int(date[0].split("-")[2]) / 1e3) < datetime.datetime.strptime(toTime, "%Y-%m-%d %H:%M:%S"):
                    with open(path + "/" + items, 'r') as fi:
                        x = json.load(fi)
                        try:
                            validityCheck = x['status']['measurer']['is_valid']
                        except (KeyError):
                            validityCheck = True
                        if validityCheck or valid == "both":
                            x.update({"date": str(datetime.datetime.fromtimestamp(int(date[0].split("-")[2]) // 1000))})
                            fromFile.append(x)
                            profilesCounter += 1
            else:  # file senza data nel nome, quindi non aggiungo il campo data
                with open(path + "/" + items, 'r') as fi:
                    x = json.load(fi)
                    fromFile.append(x)
                    profilesCounter += 1

            if profilesToCollect > 0 and profilesCounter == profilesToCollect:
                break

    return fromFile, len(fromFile)


def getMean(profile):
    size = len(profile)
    mean = 0
    for counter in range(0, size):
        mean = mean + (profile[counter] / size)
    return mean


# def sumProfiles(data):
#     # somma di profili zona per zona, ritorna un profilo nel quale ogni zona equivale alla somma
#     # delle zone dei profili passati tramite la struttura dati "data"
#
#     size = len(data)
#     for log in range(0, size - 1):
#         for value in range(0, len(data[0]['zonesProfile'])):
#             if log == 0:
#                 sumProfile.append(data[log]['zonesProfile'][value])
#             else:
#                 sumProfile[value] = sumProfile[value] + data[log]['zonesProfile'][value]
#     return sumProfile


def calcMaxByZoneWidth(profile, zoneWidth):
    # calcolo i valori massimi per larghezza delle zone e cerco il valore massimo tra tutti quelli che trovo
    calculatedProfile = list()
    size = len(profile)
    for value in range(0, size):
        zoneSum = 0
        for zone in range(0, zoneWidth):
            if (value + zone) < size:
                zoneSum = zoneSum + profile[value + zone]
            else:
                zoneSum = zoneSum + profile[value + zone - size]
        calculatedProfile.insert(value, zoneSum / zoneWidth)
    return calculatedProfile


def subtractValueFromProfile(profile, value):
    # elimino il valore "value" da tutti i punti profilo e se la differenza risulta negativa
    # sostituisco il punto con zero

    subtractedProfile = []
    size = len(profile)
    for value in range(0, size):
        if (profile[value] - value) < 0.0:
            subtractedProfile.append(0.0)
        else:
            subtractedProfile.append(profile[value] - value)
    return subtractedProfile


def subtractProfileFromProfile(profile, profileToBeSubtracted):
    # elimino il valore "value" da tutti i punti profilo e se la differenza risulta negativa
    # sostituisco il punto con zero

    subtractedProfile = []
    size = len(profile)
    for value in range(0, size):
        if (profile[value] - profileToBeSubtracted[value]) < 0.0:
            subtractedProfile.insert(value, 0.0)
        else:
            subtractedProfile.insert(value, profile[value] - profileToBeSubtracted[value])
    return subtractedProfile


def gaussFilter(profile, whichArray):
    # filtro per media zone calcolo altezza picco relativa

    filteredProfile = []
    sizeArray = len(profile)

    ###################################################################################################
    # I parametri sottostanti vengono calcolati per poter impostare flessibilmente il numero di elementi
    # del filtro.
    # IL NUMERO DI ELEMENTI DEVE COMUNQUE SEMPRE ESSERE DISPARI
    filter = []
    #    filter = [0.03, 0.1, 0.1, 0.1, 0.1, 0.1, 0.03]
    #    filter = [0.10, 0.1, 0.2, 0.2, 0.2, 0.1, 0.10]
    filterArray = [[0, 1, 0],  # 0
                   [0.2, 0.2, 0.2, 0.2, 0.2],  # 1,
                   [0.03, 0.12, 0.2, 0.3, 0.2, 0.12, 0.03],  # 2
                   [0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04,
                    0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04],  # 3
                   [0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.033,
                    0.034, 0.033,
                    0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03, 0.03],  # 4
                   [-2, -1, 0, 1, 2]]  # 5
    filter = filterArray[whichArray]
    #    filter = [1]
    lenFilter = len(filter)
    halfLenFilter = int((lenFilter - 1) / 2)
    highBound = int(lenFilter - halfLenFilter)
    lowBound = -(int(halfLenFilter))
    ###################################################################################################

    for counter in range(0, sizeArray):
        value = 0.0
        for position in range(lowBound, highBound):
            if (counter + position) < sizeArray and (counter + position) >= 0:
                value = value + profile[counter + position] * filter[position + halfLenFilter]
            elif (counter + position) < 0:
                value = value + profile[counter + position + sizeArray] * filter[position + halfLenFilter]
            elif (counter + position) >= sizeArray:
                value = value + profile[counter + position - sizeArray] * filter[position + halfLenFilter]
        filteredProfile.insert(counter, value)
    return filteredProfile


def findPeaks(profile, searchWidth, plateauSize, sort):
    peaks = {}
    sorted_peaks = {}
    profileSize = len(profile)
    previous = 0
    following = 0

    for point in range(0, profileSize):
        if point == 0:
            previous = profileSize - 1
            following = point + 1
        elif point == (profileSize - 1):
            previous = point - 1
            following = 0
        else:
            previous = point - 1
            following = point + 1

        if profile[point] > profile[previous] and profile[point] > profile[following]:
            peaks[point] = profile[point]

    if sort:
        # sorted_peaks =  sorted(peaks.items(), key=operator.itemgetter(1),reverse=True) # posizioni e valori
        sorted_peaks = sorted(peaks, key=peaks.get, reverse=True)  # solo posizioni
        return sorted_peaks
    else:
        return peaks


def findLocalMin(profile, searchWidth, numberOfFirstPeaks):
    from scipy.signal import find_peaks
    sizeArray = len(profile)
    # peaks, _ = find_peaks(profile)
    peaks = findPeaks(profile, 0, 0, False)
    firstThreePeaks = {}

    firstLocalMin = 0
    firstLocalMin2 = 0
    firstFlag = False
    secondLocalMin = 0
    secondLocalMin2 = 0
    secondFlag = False
    mean = 0.0
    amplitude = {}
    maxAmp = 0.0

    for peak in peaks:
        firstFlag = False
        if (peak - 1 >= 0):
            firstLocalMin2 = peak - 1
            firstLocalMin = peak - 1
        else:
            firstLocalMin2 = sizeArray - peak - 1
            firstLocalMin = sizeArray - peak - 1
        for firstCounter in range(peak - 1, peak - searchWidth, -1):
            if (firstCounter - 1) >= 0:
                if (profile[firstCounter]) < (profile[firstCounter - 1]) and (profile[firstCounter]) < profile[
                    firstCounter + 1]:
                    if (profile[firstCounter]) < profile[firstLocalMin]:
                        firstLocalMin = (firstCounter)
                        firstFlag = True
                #                    break
                if (profile[firstCounter]) < (profile[firstLocalMin2]):
                    firstLocalMin2 = firstCounter
            elif (firstCounter == 0):
                if (profile[0]) < (profile[sizeArray - 1]) and (profile[0]) < profile[1]:
                    if (profile[0]) < profile[firstLocalMin]:
                        firstLocalMin = (0)
                        firstFlag = True
                #                    break
                if (profile[0]) < (profile[firstLocalMin2]):
                    firstLocalMin2 = 0
            else:
                counter = sizeArray + firstCounter
                if (firstCounter + sizeArray + 1) > (sizeArray - 1):
                    counterAboveZero = 0
                else:
                    counterAboveZero = sizeArray - 1
                if (profile[counter]) < (profile[counter - 1]) and (profile[counter]) < profile[counterAboveZero]:
                    if (profile[counter]) < profile[firstLocalMin]:
                        firstLocalMin = (firstCounter)
                        firstFlag = True
                #                    break
                if (profile[counter]) < (profile[firstLocalMin2]):
                    firstLocalMin2 = counter
        secondFlag = False
        if (peak + 1 > sizeArray):
            secondLocalMin2 = peak + 1 - sizeArray
            secondLocalMin = peak + 1 - sizeArray
        else:
            secondLocalMin2 = peak + 1
            secondLocalMin = peak + 1
        for secondCounter in range(peak + 1, peak + searchWidth):
            if (secondCounter + 1) < sizeArray:
                if (profile[secondCounter]) < (profile[secondCounter - 1]) and (profile[secondCounter]) < profile[
                    secondCounter + 1]:
                    if (profile[secondCounter]) < profile[secondLocalMin]:
                        secondLocalMin = (secondCounter)
                        secondFlag = True
                #                    break
                if (profile[secondCounter]) < (profile[secondLocalMin2]):
                    secondLocalMin2 = secondCounter
            elif (secondCounter == sizeArray):
                if (profile[0]) < (profile[sizeArray - 1]) and (profile[0]) < profile[1]:
                    if (profile[0]) < profile[secondLocalMin - 1]:
                        secondLocalMin = (0)
                        secondFlag = True
                #                    break
                if (profile[0]) < (profile[secondLocalMin2 - 1]):
                    secondLocalMin2 = 0
            else:
                if (profile[secondCounter - sizeArray]) < (profile[secondCounter - sizeArray - 1]) and (
                        profile[secondCounter - sizeArray]) < profile[secondCounter - sizeArray + 1]:
                    if (profile[secondCounter - sizeArray]) < profile[secondLocalMin - 1]:
                        secondLocalMin = (secondCounter - sizeArray)
                        secondFlag = True
                #                    break
                if (profile[secondCounter - sizeArray]) < (profile[secondLocalMin2]):
                    secondLocalMin2 = secondCounter - sizeArray

        if not firstFlag:
            firstLocalMin = firstLocalMin2
        if not secondFlag:
            secondLocalMin = secondLocalMin2
        mean = (profile[firstLocalMin] + profile[secondLocalMin]) / 2
        amplitude.update({peak: round(profile[peak] - mean, 3)})

    # print('PICCHI TROVATI: ', amplitude)

    for peaksCount in range(0, numberOfFirstPeaks):
        maxAmp = 0.0
        for ampli in amplitude.keys():
            if amplitude.get(ampli) > maxAmp:
                maxAmp = amplitude.get(ampli)
                peakPos = ampli
        firstThreePeaks.update({peakPos: maxAmp})
        amplitude.pop(peakPos)

    return firstThreePeaks


def calcCC(firstProfile, secondProfile, offsetRange):
    # calcolo la correlazione incrociata tra due profili

    offset = 0
    max = 0.0
    autoCorrelationValues = []
    autoCorrelationData = {}
    sortedAutoCorrelationData = []
    size = len(firstProfile)

    for position in range(0, size):
        for counter in range(0, size):
            if counter == 0:
                if (counter + position < size):
                    autoCorrelationValues.insert(position, firstProfile[counter] * secondProfile[counter + position])
                else:
                    autoCorrelationValues.insert(position,
                                                 firstProfile[counter] * secondProfile[counter + position - size])
            else:
                if (counter + position < size):
                    autoCorrelationValues[position] = autoCorrelationValues[position] + (
                            firstProfile[counter] * secondProfile[counter + position])
                else:
                    autoCorrelationValues[position] = autoCorrelationValues[position] + (
                            firstProfile[counter] * secondProfile[counter + position - size])
        autoCorrelationData[str(position)] = autoCorrelationValues[position]

    sortedAutoCorrelationData = findPeaks(autoCorrelationValues, 0, 0, True)

    for plausibleOffset in sortedAutoCorrelationData:
        if (plausibleOffset > 180 and (plausibleOffset > 359 - offsetRange)):
            offset = plausibleOffset - 360
            break
        elif (plausibleOffset < offsetRange):
            offset = plausibleOffset
            break

    sortedAutoCorrelationData = sorted(autoCorrelationData, key=autoCorrelationData.get, reverse=False)
    return sortedAutoCorrelationData, max, offset, autoCorrelationValues


def findPeaksByDistance(profile, distance):
    # restituisce coppie di valori a distanze date. Le distanze vengono passate tramite la lista "distance"
    # le coppie di valori vengono restituite come chiavi di un dictionary, la distanza come valore relativo alla chiave:
    # se vn --> valore n-esimo e dn --> distanza n-esima
    # allora key:value --> '(v1,v2)':d1

    from scipy.signal import find_peaks
    peaksFound = {}
    peaks, _ = find_peaks(profile)
    for dist in distance:
        for firstPeak in peaks:
            for secondPeak in peaks:
                if (abs(firstPeak - secondPeak)) == dist:
                    peaksFound.setdefault(dist, {}).setdefault(firstPeak, {})[firstPeak] = secondPeak

    return peaksFound


def findPeaksByDistanceWithTolerance(profile, distance):
    # 18/09/20 NON FUNZIONA COME ATTESO
    peaksFound = {}
    counter = 0
    distanceSize = len(distance)

    while not bool(peaksFound):
        peaksFound = findPeaksByDistance(profile, distance)
        print(bool(peaksFound))
        if counter < distanceSize:
            distance[counter] = distance[counter] + 1
            counter = counter + 1
            print(distance)
        else:
            counter = 0
            distance[counter] = distance[counter] + 1
            print(distance)

    return peaksFound, distance


def zonesToRaw(profile, isRaw=True):
    rawProfile = []
    expandedZoneProfile = []
    outProfile = []
    degrees = 360
    numberOfZones = len(profile['values'])
    MCD = int(math.gcd(numberOfZones, degrees))
    MCM = int((numberOfZones * degrees) / MCD)
    zonesToDegrees = int(MCM / degrees)
    degreesToZones = int(MCM / numberOfZones)

    # espando il profilo a zone in un profilo che ha MCM posizioni
    for position in range(0, numberOfZones):
        for counter in range(0, degreesToZones):
            expandedZoneProfile.insert(int(counter + (position * degreesToZones)), profile['values'][position])

    # riporto il profilo di MCM posizioni, mediato, su di un profilo a 360 posizioni
    valueToPut = 0
    for position in range(0, degrees):
        valueToPut = 0
        for counter in range(0, zonesToDegrees):
            valueToPut += expandedZoneProfile[int(counter + (position * zonesToDegrees))]

        rawProfile.insert(int(position), valueToPut / zonesToDegrees)

    if not isRaw:
        for value in profile['values']:
            outProfile.append(value)
        return outProfile

    return rawProfile


def shiftProfile(profile, shift):
    shiftedProfile = []
    profileSize = len(profile)

    for index in range(shift, profileSize + shift):
        if index < profileSize:
            shiftedProfile.insert(index - shift, profile[index])
        elif index == profileSize:
            shiftedProfile.insert(index - shift, profile[0])
        else:
            shiftedProfile.insert(index - shift, profile[index - profileSize])

    return shiftedProfile


def errorFromSP(profile, thicknessSP, isRaw=True, kind=0):
    outProfile = []
    profileKind = ''
    for thicknessPV in profile:
        if kind == 2:  # valori profilo assoluti
            outProfile.append(thicknessPV)
        elif kind == 1:  # scostamenti da SP in micron
            outProfile.append(thicknessPV - thicknessSP)
        else:  # scostamenti da SP percentuali
            outProfile.append(((thicknessPV - thicknessSP) / thicknessSP) * 100)

    return outProfile


def printStats(profile, ptPdf):
    txt = []
    try:
        validityCheck = not profile['status']['measurer']['is_valid']
    except (KeyError):
        validityCheck = None
    try:
        sigma_abs = round(profile['stats']['statsProfileZone']['sigma_abs'], 2)
    except (KeyError):
        sigma_abs = round(profile['stats']['sigma_abs'], 2)
    try:
        sigma_perc = round(profile['stats']['statsProfileZone']['sigma_perc'], 2)
    except (KeyError):
        sigma_perc = round(profile['stats']['sigma_perc'], 2)

    if ptPdf:
        txt.append("DATA/ORA           : " + str(profile['date']))
        txt.append("MIS                : " + str(profile['status']['measurer']['direction']))
        txt.append("STIRO              : " + str(profile['status']['hauloff']['direction']))
        txt.append("REG. AUTO          : " + str(profile['status']['application']['actuation_active']))
        txt.append("LINE SPEED (m/min) : " + str(profile['production']['line_speed']))
        txt.append("THICKNESS (µm)     : " + str(profile['production']['thickness']))
        txt.append("LAYFLAT (mm)       : " + str(profile['production']['layflat']))
        txt.append("2sigma (%)         : " + str(sigma_perc))
        txt.append("2sigma (µm)        : " + str(sigma_abs))
    else:
        print("DATA:", profile['date'], "MIS:", profile['status']['measurer']['direction'], "STIRO:",
              profile['status']['hauloff']['direction'], "SCARTATO:", validityCheck, " REG. AUTO:",
              profile['status']['application']['actuation_active'])
        print("LINE SPEED (m/min):", profile['production']['line_speed'], "## THICKNESS (µm):",
              profile['production']['thickness'], "## LAYFLAT (mm):", profile['production']['layflat'], )
        print("2sigma (%):", sigma_perc, "2sigma (µm):", sigma_abs)

    return txt


def print_graph(pR1, pR2, pR3, rawPR, ticks, barGraf, txt, color="None", pdfPoint=None):
    top_page = 0.75
    margin_page = 0.10
    line_height = 0.07
    fig, host = plt.subplots()
    plt.grid()
    par1 = host.twinx()
    par1.set_ylim(ymin=0, ymax=100)
    par1.tick_params(axis='y', colors='red')
    plt.xticks([i for i in range(0, 360, 5)])

    host.tick_params(axis='y', colors='blue')

    if not color == "None":
        host.set_facecolor(color)

    if barGraf:
        par1.bar(ticks, pR1, color="none", edgecolor="r")
        host.plot(ticks, pR2, "blue")
        host.plot(ticks, pR3, "black")
    else:
        plot1 = par1.plot(pR1, "r")
        plot2 = host.plot(pR2, "blue")
        plot3 = host.plot(pR3, "black")

    if not pdfPoint == None:
        firstPage = plt.figure(figsize=(12, 2))
        firstPage.clf()
        for testo in txt:
            firstPage.text(margin_page, top_page, testo, transform=firstPage.transFigure, size=9,
                           fontfamily='monospace')
            top_page -= line_height

        pdfPoint.savefig(firstPage)
        pdfPoint.savefig(fig)
        plt.close(firstPage)
    else:
        plt.show()

    plt.close(fig)


def print_stat_graph(gCounter, tkSize, title, yLabel, xData, yData, ptPdf=None, yLim=0):
    fig = plt.figure(gCounter)
    plt.xticks(fontsize=tkSize)
    fig.suptitle(title, fontsize=14)
    plt.ylabel(yLabel)
    plt.grid()
    if yLim > 0:
        axes = plt.gca()
        axes.set_ylim([0, yLim])

    plt.plot(xData, yData)
    if not ptPdf == None:
        ptPdf.savefig(fig)

    plt.close(fig)
    return gCounter + 1
