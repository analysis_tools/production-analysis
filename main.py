import methods as m
from datetime import datetime
from matplotlib import pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages
import yaml

interfacciaGrafica = False

# TEST DI INTERFACCIA GRAFICA ---- INIZIO
if interfacciaGrafica:
    from tkinter import filedialog
    from tkinter import *
    import tkinter as tk


    def browse_button():
        # Allow user to select a directory and store it in global var
        # called folder_path
        # global pathFile
        global folder_path
        filename = filedialog.askdirectory()
        # pathFile = filename
        folder_path.set(filename)
        print(filename)
# TEST DI INTERFACCIA GRAFICA ---- FINE


# Loading parameters
parameters_file = open("config.yml", "r")
parameters = yaml.load(parameters_file, Loader=yaml.FullLoader)

# parametrizzazione analisi da file
if interfacciaGrafica:
    pathFile = None
else:
    pathFile = parameters.get("logs_path")

start = str(parameters.get("start_date_hour"))
stop = str(parameters.get("stop_date_hour"))
kindOf = parameters.get("profiles")
how_many = parameters.get("how_many_profiles_to_analize")
profileFreq = parameters.get("profileFreq")
barGraph = parameters.get("barGraph")
printToPdf = parameters.get("printToPdf")
absolute = parameters.get("absolute")
tuttiIprofili = parameters.get("tuttiIprofili")
rawProfile = parameters.get("rawProfile")
printProfiles = parameters.get("printProfiles")

# TEST DI INTERFACCIA GRAFICA ---- INIZIO
if interfacciaGrafica:
    root = Tk()
    folder_path = StringVar()
    frame_a = tk.Frame()
    frame_b = tk.Frame()
    # frame_c = tk.Frame()
    # frame_d = tk.Frame()
    # frame_e = tk.Frame()
    lbl1 = Label(master=frame_a, textvariable=folder_path)
    lbl1.grid(row=0, column=1)
    button2 = Button(master=frame_a, text="Browse", command=browse_button)
    button2.grid(row=0, column=3)
    lbl2 = Label(master=frame_b, text="Start")  # , width=10, height=2, anchor="w")
    lbl2.grid(row=1, column=0)
    txtBox1 = Entry(master=frame_b)  # , width=10)
    txtBox1.grid(row=1, column=3)
    # lbl3 = Label(master=frame_e,text="")#, width=2, height=2)
    # lbl3.grid(row=1, column=4)
    frame_a.pack()
    frame_b.pack()
    # frame_c.pack()
    # frame_d.pack()
    # frame_e.pack()

    root.mainloop()

    pathFile = folder_path.get()
    print("pathfile:", pathFile)
# TEST DI INTERFACCIA GRAFICA ---- FINE

# seleziono i profili in un intervallo temporale
# in teoria, mettendo "0" e "0", l'intervallo temporale non viene considerato ma si prendono tutti i file in cartella
# ovviamente i path (che trovate sopra nei metodi) vanno adattati al percorso nel quale tenete i log
try:
    profilesTest, conteggio = m.getDataFromFile(pathFile, start, stop, tuttiIprofili, how_many)  # jolly 7/05
except:
    print("Il percorso selezionato non esiste:")
    print(pathFile)
    exit(11)

profilesToAnalyze = []
thickness = []
dates = []
layflat = []
flowrate = []
twoSigma = []
large_gain = []
matrix_major = []
matrix_minor = []
profiles_to_average = []
measurer_speed = []
graphTicks = []  # numero di punti per asse X, mi serve quando sono uso il grafico a barre
lineZero = []  # linea retta di valore zero, serve per visualizzare meglio l'andamento del profilo
profiles_txt = []  # stringhe statistiche grafici
production_txt = []  # stringhe statistiche intervallo temporale completo

# inizializzazione variabili
colorGraph = "None"  # colore dello sfondo del grafico che viene selezionato in base a scartato, sigma Min, sigma Max
gFilterIndex = 0  # indice del filtro gaussiano utilizzato
countProfile = 0  # contatore incrementale dei profili processati
countTicks = 0  # contatore punti profilo
pp = None  # puntatore al file pdf con i grafici
sigmaMax = 0  # per individuare il profilo con il 2sigma maggiore all'interno del periodo
sigmaMin = 100  # per individuare il profilo con il 2sigma minore all'interno del periodo
timeSigmaMax = "0"  # registro il time stamp del max
timeSigmaMin = "0"  # registro il time stamp del min

if rawProfile:
    profileKind = 'rawProfile'
else:
    profileKind = 'zonesProfile'

# profilesToAnaliyze è la selezione di profili sui quali abbiamo fatto effettivamente la ricerca di autoallineamento
# per analizzare tutti i profili sostituire con profilesTest
for profile in profilesTest:
    try:
        validityCheck = profile['status']['measurer']['is_valid']
    except KeyError:
        validityCheck = False
    if validityCheck or tuttiIprofili == "both":
        try:
            twoSigma.append(profile['stats']['statsProfileZone']['sigma_perc'])
            # twoSigma.append(2 * np.std(profile['rawProfile'] / np.mean(profile['rawProfile']) * 100))
            # stampa a video il max e il min all'interno della produzione selezionata (scommentare sotto la stampa)
            # if profile['stats']['statsProfileZone']['sigma_perc'] > sigmaMax:
            #     sigmaMax = profile['stats']['statsProfileZone']['sigma_perc']
            #     timeSigmaMax = datetime.strptime(profile['date'], '%Y-%m-%d %H:%M:%S')
            # elif profile['stats']['statsProfileZone']['sigma_perc'] < sigmaMin:
            #     sigmaMin = profile['stats']['statsProfileZone']['sigma_perc']
            #     timeSigmaMin = datetime.strptime(profile['date'], '%Y-%m-%d %H:%M:%S')

        except KeyError:
            twoSigma.append(profile['stats']['sigma_perc'])
        #     else:
        #         twoSigma.append(np.mean(twoSigma))
        thickness.append(profile['production']['thickness'])
        dates.append(datetime.strptime(profile['date'], '%Y-%m-%d %H:%M:%S'))
        layflat.append(profile['production']['layflat'])
        flowrate.append(profile['production']['flowrate'])
        large_gain.append(profile['parameters']['large_gain'])
        # matrix_major.append(profile['parameters']['matrix_major'])
        # matrix_minor.append(profile['parameters']['matrix_minor'])
        measurer_speed.append(profile['status']['measurer']['speed'])
        try:
            profiles_to_average.append(profile['parameters']['profiles_to_average'])
        except KeyError:
            profiles_to_average.append(0)

if False:  # stampa a video il max e il min all'interno della produzione selezionata (scommentare sopra l'analisi)
    print("sigma Max: ", round(sigmaMax, 2))
    print("time:     ", timeSigmaMax)
    print("sigma Min: ", round(sigmaMin, 2))
    print("time:     ", timeSigmaMin)

# plot dei grafici e dell'attuazione relativa

if conteggio > 0:
    twoSigmaMean = np.mean(twoSigma)
    twoSigmaMin = np.min(twoSigma)
    twoSigmaMax = np.max(twoSigma)
    twoSigmaStd = np.std(twoSigma)
else:
    profilesTest, conteggio = m.getDataFromFile(pathFile, "0", "0", tuttiIprofili, 0)
    if conteggio > 0:
        print("Non ci sono profili da analizzare nell'intervallo temporale selezionato")
        print("start    : ", start)
        print("stop     : ", stop)
        print("Intervallo DISPONIBILE")
        print("start    : ", profilesTest[0]["date"])
        print("stop     : ", profilesTest[conteggio - 1]["date"])
        print("# profili: ", conteggio)
        exit(9)
    else:
        print("Non ci sono profili da analizzare al percorso selezionato")
        print(pathFile)
        exit(10)

if printToPdf:
    production_txt.append("PRODUCTION STATS: ------------------------")
    production_txt.append("2Sigma samples : " + str(len(twoSigma)))
    production_txt.append("2Sigma medio   : " + str(round(twoSigmaMean, 2)))
    production_txt.append("2Sigma min     : " + str(round(twoSigmaMin, 2)))
    production_txt.append("2Sigma max     : " + str(round(twoSigmaMax, 2)))
    production_txt.append("2Sigma dev     : " + str(round(twoSigmaStd, 2)))
    production_txt.append("------------------------------------------")
else:
    print("PRODUCTION STATS: ------------------------")
    print("2Sigma samples :", len(twoSigma))
    print("2Sigma medio   :", round(twoSigmaMean, 2))
    print("2Sigma min     :", round(twoSigmaMin, 2))
    print("2Sigma max     :", round(twoSigmaMax, 2))
    print("2Sigma dev     :", round(twoSigmaStd, 2))
    print("------------------------------------------")
    print("                                          ")

# impostazione tick asse x grafici
tickSize = 12

# impaginazione dati
top_page_stat = 0.75
line_height_stat = 0.07
margin_page_stat = 0.10

if printToPdf:
    pp = PdfPages(pathFile + '/' + str(datetime.now().strftime("%Y%m%d_%H%M%S")) + '_grafici' + '.pdf')
    firstPageStat = plt.figure(figsize=(12, 2))
    firstPageStat.clf()
    for texto in production_txt:
        firstPageStat.text(margin_page_stat, top_page_stat, texto, transform=firstPageStat.transFigure, size=9,
                           fontfamily='monospace')
        top_page_stat -= line_height_stat

    pp.savefig(firstPageStat)
    plt.close(firstPageStat)

plotCounter = 1

plotCounter = m.print_stat_graph(plotCounter, tickSize, 'Thickness', "µm", dates, thickness, ptPdf=pp)
plotCounter = m.print_stat_graph(plotCounter, tickSize, '2σ', "%", dates, twoSigma, ptPdf=pp,
                                 yLim=0)
plotCounter = m.print_stat_graph(plotCounter, tickSize, 'Measurer Speed', "#", dates, measurer_speed, ptPdf=pp)
plotCounter = m.print_stat_graph(plotCounter, tickSize, 'Layflat', "mm", dates, layflat, ptPdf=pp)
plotCounter = m.print_stat_graph(plotCounter, tickSize, 'Flowrate', "kg/h", dates, flowrate, ptPdf=pp)

# plotCounter = print_stat_graph(plotCounter, tickSize, 'Layflat', "mm", dates, layflat, ptPdf=printToPdf)
# plotCounter = print_stat_graph(plotCounter, tickSize, 'Large Gain', "%", dates, large_gain, ptPdf=printToPdf)
# plotCounter = print_stat_graph(plotCounter, tickSize, 'Matrix Major', "#", dates, matrix_major, ptPdf=printToPdf)
# plotCounter = print_stat_graph(plotCounter, tickSize, 'Matrix Minor', "#", dates, matrix_minor, ptPdf=printToPdf)
# plotCounter = print_stat_graph(plotCounter, tickSize, 'Profiles to Average', "#", dates, profiles_to_average,
#                                ptPdf=printToPdf)

if not printToPdf:
    plt.show()

if printProfiles:
    for profile in profilesTest:
        graphTicks = []
        lineZero = []
        length = len(profile[profileKind])
        for countTicks in range(length):
            graphTicks.append(countTicks)
            if absolute == 2:
                lineZero.append(float(profile['production']['thickness']))
            else:
                lineZero.append(0.0)

        printGraph = False
        thicknessSP = profile['production']['thickness']
        try:
            sigma_perc = profile['stats']['statsProfileZone']['sigma_perc']
        except KeyError:
            sigma_perc = profile['stats']['sigma_perc']

        # plot dei grafici e dell'attuazione relativa
        if countProfile % profileFreq == 0 and sigma_perc != twoSigmaMax and sigma_perc != twoSigmaMin:
            printGraph = True
            try:
                validityCheck = profile['status']['measurer']['is_valid']
            except KeyError:
                validityCheck = False
            if not validityCheck:
                colorGraph = "yellow"
            else:
                colorGraph = "white"
        elif sigma_perc > (2 * twoSigmaMean) and sigma_perc != twoSigmaMax:
            printGraph = True
            colorGraph = "moccasin"
            # print("2sigma out of range")
        elif sigma_perc == twoSigmaMin:
            printGraph = True
            colorGraph = "palegreen"
            if not printToPdf:
                print("2sigma MIN")
        elif sigma_perc == twoSigmaMax:
            printGraph = True
            colorGraph = "mistyrose"
            if not printToPdf:
                print("2sigma MAX")

        if printGraph:
            profiles_txt = m.printStats(profile, printToPdf)
            m.print_graph(m.zonesToRaw(profile['actuators'], rawProfile),
                          m.errorFromSP(m.gaussFilter(profile[profileKind], gFilterIndex), thicknessSP, kind=absolute),
                          lineZero, rawProfile, graphTicks, barGraph, profiles_txt, colorGraph, pp)
        countProfile += 1

if printToPdf:
    pp.close()
